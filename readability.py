#https://spacy.io/usage/models
#https://readable.io/blog/the-flesch-reading-ease-and-flesch-kincaid-grade-level/
#http://www.stylewriter-usa.com/stylewriter-editing-readability.php
#https://rdrr.io/cran/koRpus/man/readability-methods.html
#python -m spacy download en
from collections import  Counter
import textstat
import sys ,os
import re
import pyphen 

num_2syllables=0
num_complex_syllables=0
syllub="-"
regex = r"[A-Z][\w\s\d\,\'\;\:\"\(\)\[\]\%\$\!\?]*(\.)"
pyphen.language_fallback('nl_NL_variant1')
dic = pyphen.Pyphen(lang='nl_NL')
long_sentence_limit=35
fn = sys.argv[1]
f = open(fn, "r")
string =f.read()
string = string.rstrip().lstrip()
SentenceCount=len(list(re.finditer(regex, string)))
while "  " in string: 
       str
# Average length of sentence
ALS= float(textstat.lexicon_count(string)/textstat.sentence_count(string))
# A compnent of BOG Index
BOG_sentence= (ALS*ALS)/long_sentence_limit
# A compnent of BOG Index
BOG_word = ((abbr+textstat.polysyllabcount(string)+textstat.difficult_words(string))*250)/(textstat.lexicon_count(string))
# Abbrivation is a word with all capital letters and the lenght more than two
abbr=0
for word in string.split():
  if word.isupper():
        if len(word) >=2:
           count2=0
           for i in word:
              if(i.isupper()):
                count2=count2+1
           if count2>=2:
                      abbr=abbr+1
# calculating the number of 2-sylables words and complex words(more than 2-sylables)
# using pyphen library
with open(fn) as f:
    for line in f:
        for word in re.findall(r'\w+', line):            
              phen=   dic.inserted(word)
              occur=phen.count(syllub)
              if occur ==1:
                 num_2syllables=num_2syllables+1
              if occur >1:
                 num_complex_syllables=num_complex_syllables+1
# calculating the total num of syllables and total num words			
num_syllables=0
sum_all_syllables=0
number_of_words=0
with open(fn) as f:
    for line in f:
        for word in re.findall(r'\w+', line):
              phen = dic.inserted(word)
              occur= phen.count(syllub)
              num_syllables = occur+1
              sum_all_syllables = sum_all_syllables + num_syllables
              number_of_words=number_of_words+1			
# flesch rea index 
fleschReadingEase = 206.835-1.015*ALS-84.6*(float(sum_all_syllables/number_of_words))
fleschKincaidGradeLevel = 0.39*ALS+11.8*(sum_all_syllables/number_of_words)-15.59
# the num of numeric words in text	
ncount = len(re.findall("[0-9]", string))
'''
pcount:
number of punctuation marks includes the comma,  semicolon,
colon, exclamation mark, question mark and dots excluding 
the dots used in float numbers so instead of counting 
exclamation mark, question mark  and dots separately the
total number of sentences is considered'''
pcount = len(re.findall("[,:;]", string))
# ................... LIX INDEX .........................
# lix index = (#words / #periods) + 100* #longword /#words
counter = Counter(string)
colon=counter[':']
# the long word means the words with length more than 6 letters
longword = 0
for word in string.split():
  if len(word)>=6:
   longword=longword+1
A = number_of_words
# the period in text means the total number of sentence plus colon
# B = number of periods
''' number of  periods is calculated using the number of sentence obtained by library  text stat'''
B1 = colon + textstat.sentence_count(string)
lix1 = float(A/B1)+float(longword*100)/A
''' number of  periods is calculated using the number of sentence obtained by library  ordinary library in python'''
B2 = SentenceCount+colon
lix2 = float(A/B2)+float(longword*100)/A
# ................... LIX INDEX .........................
print(" these parameters are calculated using python ordinary library")
print ("flesch Reading Ease: "+str(f1leschReadingEase))
print ("flesch Kincaid Grade Level: "+str(fleschKincaidGradeLevel))
print ("number of syllubs: "+str(sum_all_syllables))
print ("alphabetic words: "+str(number_of_words))
print ("sentences: "+str( SentenceCount))
print ("puncuation mark: "+str(pcount))
print ("numeric words: "+str(ncount))
print("average length of sentence: "+str(ALS))
print("BOG of sentence: "+str(BOG_sentence))
print("BOG of word: "+str(BOG_word))
print("number of 2 syllables: "+str(num_2syllables))
print("number of complex word: "+str(num_complex_syllables))
print("number of abbr: "+str(abbr))
print("calculating the factors and components of different indices using textstat library")
print ("char_count: "+str( textstat.char_count(string)))
print ("count_spaces: "+str( textstat.char_count(string , ignore_spaces=False)))
print ("letter_count: "+str( textstat.letter_count(string)))
print("count_spaces: "+str( textstat.letter_count(string, ignore_spaces=False)))
print ("lexicon_count: "+str( textstat.lexicon_count(string)))
print ("count_puncuation: "+str( textstat.lexicon_count(string, removepunct=False)))
print("syllab_count: "+str( textstat.syllable_count(string)))
print ("word_count: "+str( textstat.lexicon_count(string)))
print ("sentance_count: "+str( textstat.sentence_count(string)))
print ("average sentance_length: "+str( textstat.avg_sentence_length(string)))
print ("avgerage syllables per word: "+str( textstat.avg_syllables_per_word(string)))
print ("average letters per word: "+str( textstat.avg_letter_per_word(string)))
print ("average wordrs per sentance: "+str( textstat.avg_sentence_per_word(string)))
print("flesch reading-ease: "+str( textstat.flesch_reading_ease(string)))
print("flesch kincaid grade: "+str(textstat.flesch_kincaid_grade(string)))
print ("poly sylables: " + str( textstat.polysyllabcount(string)))
print ("index_smog: "+str(textstat.smog_index(string)))
print ("index_liau: "+str( textstat.coleman_liau_index(string)))
print ("automated_index_readability: "+str( textstat.automated_readability_index(string)))
print ("result_linser: "+str( textstat.linsear_write_formula(string)))
print ("standard: " + str(textstat.text_standard(string)))
print ("score_fog: "+ str(textstat.gunning_fog(string)))
print ("score_lix: "+ str(textstat.lix(string)))
print ("score_dale_chall: "+str( textstat.dale_chall_readability_score(string)))
print ("result_difficult words: "+str( textstat.difficult_words(string)))
print ("score_lix: "+ str(lix))
#---------------ALS= #word/#sentence------------------
Rix = longword/textstat.sentence_count(string)
#Texts with a RIX < 1.8 are considered very easy, around 3.7 normal, and > 7.2 very difficult to read.
print ("Rix Index: "+ str(Rix))




















